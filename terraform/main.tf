terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

provider "yandex" {
  token     = "y0_AgAAAAABsTeZAATuwQAAAAD2Qw97DS1l49xnSDC1jQ40bt8Ez_Jps9k"
  cloud_id  = "b1ga7ra6ipv8mecnovt3"
  folder_id = "b1gfhn8fbprngkrtr1kp"
  zone      = "ru-central1-a"
}

data "yandex_compute_image" "my-ubuntu-2004-1" {
  family = "ubuntu-2004-lts"
}

resource "yandex_compute_instance" "my-vm-2" {
  name        = "my-nginx-1"
  platform_id = "standard-v1"
  zone        = "ru-central1-a"

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = "${data.yandex_compute_image.my-ubuntu-2004-1.id}"
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.my-sn-1.id
    nat = true
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_ed25519.pub")}"
    user-data = "${file("user_data.sh")}"
  }
}

resource "yandex_compute_instance" "my-vm-3" {
  name        = "my-nginx-2"
  platform_id = "standard-v1"
  zone        = "ru-central1-a"

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = "${data.yandex_compute_image.my-ubuntu-2004-1.id}"
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.my-sn-1.id
    nat = true
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_ed25519.pub")}"
    user-data = "${file("user_data.sh")}"
  }
}

resource "yandex_vpc_network" "default" {
  name = "my-nw-2"
}

resource "yandex_vpc_subnet" "my-sn-1" {
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.default.id
  v4_cidr_blocks = ["192.168.10.0/24"]
}

resource "yandex_lb_target_group" "my-lb-tg-1" {
  name      = "my-target-group-1"
  region_id = "ru-central1"

  target {
    subnet_id = "${yandex_vpc_subnet.my-sn-1.id}"
    address   = "${yandex_compute_instance.my-vm-2.network_interface.0.ip_address}"
  }

  target {
    subnet_id = "${yandex_vpc_subnet.my-sn-1.id}"
    address   = "${yandex_compute_instance.my-vm-3.network_interface.0.ip_address}"
  }
}

resource "yandex_lb_network_load_balancer" "my-nw-lb-1" {
  name = "my-network-load-balancer-1"

  listener {
    name = "my-listener-1"
    port = 80
  }

  attached_target_group {
    target_group_id = "${yandex_lb_target_group.my-lb-tg-1.id}"

    healthcheck {
      name = "http"
      http_options {
        port = 80
      }
    }
  }
}

resource "yandex_compute_instance" "my-webserver" {
  name        = "reactjs-server"
  platform_id = "standard-v1"
  zone        = "ru-central1-a"

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = "${data.yandex_compute_image.my-ubuntu-2004-1.id}"
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.my-sn-1.id
    nat = true
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_ed25519.pub")}"
  }
}

output "internal_ip_address_my-webserver" {
  value = yandex_compute_instance.my-webserver.network_interface.0.ip_address
}

output "external_ip_address_my-webserver" {
  value = yandex_compute_instance.my-webserver.network_interface.0.nat_ip_address
}

output "internal_ip_address_vm_2" {
  value = yandex_compute_instance.my-vm-2.network_interface.0.ip_address
}

output "external_ip_address_vm_2" {
  value = yandex_compute_instance.my-vm-2.network_interface.0.nat_ip_address
}

output "internal_ip_address_vm_3" {
  value = yandex_compute_instance.my-vm-3.network_interface.0.ip_address
}

output "external_ip_address_vm_3" {
  value = yandex_compute_instance.my-vm-3.network_interface.0.nat_ip_address
}

output "my_balancer_ip_address" {
  value = yandex_lb_network_load_balancer.my-nw-lb-1.listener
}
